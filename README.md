# Nvim Treesitter Rust Extra

Adds a few extra highlight groups to rust that are not upstreamable due to not
having a highlight group for builtin types etc.

See `queries/rust/highlights.scm` for the new highlighting rules that a color scheme
needs to add
