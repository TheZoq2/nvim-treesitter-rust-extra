;;extends

(type_arguments  ["<" ">"] @TSRustGeneric)
(type_parameters ["<" ">"] @TSRustGeneric)

((identifier) @type.builtin
 (#any-of?
    @type.builtin
    "Result"
    "Option"
    "Vec"
    "Box"
    "Clone"
    "Iterator"
    "IntoIterator"
    "DoubleEndedIterator"
    "ExactSizeIterator"
    "String"
    "ToString"
    "TryFrom"
    "TryInto"))

((type_identifier) @type.builtin
 (#any-of?
    @type.builtin
    "Result"
    "Option"
    "Vec"
    "Box"
    "Clone"
    "Iterator"
    "IntoIterator"
    "DoubleEndedIterator"
    "ExactSizeIterator"
    "String"
    "ToString"
    "TryFrom"
    "TryInto"))
